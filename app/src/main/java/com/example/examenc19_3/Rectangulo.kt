package com.example.examenc19_3

class Rectangulo {

    var num1:Int = 0
    var num2:Int = 0

    constructor(num1:Int, num2:Int){
        this.num1 = num1;
        this.num2 = num2;
    }

    fun CalcularPerimetro():Int{
        return num2 + num2 + num2 + num2;
    }

    fun CalcularArea():Int{
        return num1 * num2;
    }
}