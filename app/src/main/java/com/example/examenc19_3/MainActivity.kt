package com.example.examenc19_3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private lateinit var btnEntrar: Button;
    private lateinit var btnSalir: Button;
    private lateinit var txtNombre: EditText;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        btnEntrar.setOnClickListener{ btnEntrar() }
        btnSalir.setOnClickListener{ btnSalir() }
    }

    private fun iniciarComponentes(){
        btnEntrar = findViewById(R.id.btnEntrar)
        btnSalir = findViewById(R.id.btnSalir)
        txtNombre = findViewById(R.id.txtNombre)
    }

    private fun btnEntrar(){
        var strNombre:String

        //Asignar Los Strings qu se declararon en R.Valus
        strNombre = application.resources.getString(R.string.Nombre)

        if(txtNombre.text.toString().equals(strNombre)){
            //Hacer paquete de datos que enviar
            var bundle = Bundle();
            bundle.putString("nombre",strNombre)
            var intent = Intent(this@MainActivity,RectanguloActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
        } else{
            Toast.makeText(this.applicationContext,"Nombre no valido",
                Toast.LENGTH_LONG).show()
        }
    }

    private fun btnSalir(){
        finish()
    }
}