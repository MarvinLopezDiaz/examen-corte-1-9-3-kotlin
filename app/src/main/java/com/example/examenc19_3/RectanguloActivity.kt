package com.example.examenc19_3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import android.widget.Toast

class RectanguloActivity : AppCompatActivity() {
    private lateinit var btnCalcular: Button;
    private lateinit var btnLimpiar: Button;
    private lateinit var btnRegresar: Button;

    private lateinit var txtNum1: EditText;
    private lateinit var txtNum2: EditText;
    private lateinit var lblPerimetro: TextView;
    private lateinit var lblArea: TextView;
    private var rectangulo = Rectangulo(0,0);

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rectangulo)
        iniciarComponentes()
        btnCalcular.setOnClickListener{ btnCalcular()}
        btnLimpiar.setOnClickListener{ Limpiar()}
        btnRegresar.setOnClickListener{ regresar()}
    }

    fun Limpiar(){
        txtNum1.setText("")
        txtNum2.setText("")
        txtNum1.requestFocus()
        txtNum2.requestFocus()
        lblPerimetro.setText("")
        lblArea.setText("")
    }

    private fun regresar(){
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Calculadora")
        confirmar.setMessage(" ¿Desea regresar? ")
        confirmar.setPositiveButton("Confirmar"){
                dialogInterface,which->finish()
        }
        confirmar.setNegativeButton("Cancelar"){
                dialogInterface, which->}.show()
    }

    private fun iniciarComponentes (){
        btnCalcular = findViewById(R.id.btnCalcular)

        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)
        txtNum1 = findViewById(R.id.txtNum1)
        txtNum2 = findViewById(R.id.txtNum2)
        lblPerimetro = findViewById(R.id.lblPerimetro)
        lblArea = findViewById(R.id.lblArea)
    }

    private fun btnCalcular(){
        val text1 = txtNum1.text.toString()
        val text2 = txtNum2.text.toString()

        if(!TextUtils.isEmpty(text1) && !TextUtils.isEmpty(text2)){
            rectangulo.num1 = txtNum1.text.toString().toInt()
            rectangulo.num2 = txtNum2.text.toString().toInt()
            lblPerimetro.text = rectangulo.CalcularPerimetro().toString()
            lblArea.text = rectangulo.CalcularArea().toString()
        } else{
            Toast.makeText(this.applicationContext,"Falta rellenar campos",
                Toast.LENGTH_LONG).show()
        }
    }
}